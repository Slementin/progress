import { Component } from '@angular/core';

import { ProgressProvider } from '../../providers/progress-provider';

@Component({
  selector: 'progress-indicator',
  templateUrl: 'progress-indicator.html'
})
export class ProgressIndicator {

  constructor(public progressProvider: ProgressProvider) {}

}
