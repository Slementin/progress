import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, reorderArray } from 'ionic-angular';
import { TaskProvider } from '../../providers/task-provider';
import { ProgressProvider } from '../../providers/progress-provider';

import { Task } from '../../app/task';

@Component({
  selector: 'page-task-details',
  templateUrl: 'task-details.html'
})
export class TaskDetails {

  taskDetails: Task;

  notEditable: boolean;
  noSubtasks: boolean;

  subtaskPoints: number;

  editButtonText: string;
  adderPlaceholder: string;

  constructor(public navCtrl: NavController, private navParams: NavParams, private taskProvider: TaskProvider, private progressProvider: ProgressProvider, public toastCtrl: ToastController) { }

  ionViewDidLoad() {
    this.taskDetails = this.navParams.get('task');

    this.adderPlaceholder = 'New item ...'
    this.notEditable = true;
    this.editButtonText = 'Edit';
    this.subtaskPoints = this.taskProvider.subtaskPoints;

    this.checkSubtaskAdder();
  }

  // If there are no subtasks, show the subtask adder.
  checkSubtaskAdder() {
    if (this.taskDetails.subtasks.length < 1) {
      this.noSubtasks = true;
    } else {
      this.noSubtasks = false;
    }
  }

  // Hide placeholder when input is focused.
  inputFocused() {
    this.adderPlaceholder = '';
  }

  // Add subtask when the textfield is blurred. Set placeholder.
  inputBlurred(item) {
    if (item.value) {
      this.addSubtask(item.value);
      item.value = '';
    }
    this.adderPlaceholder = 'New item ...'
  }

  // Add subtask when the user hits enter.
  enterTapped(item) {
    let text = item.value;
    // Remove whitespace from the input text.
    let cleanedText = text.replace(/^\s+|\s+$/g, '');
    this.addSubtask(cleanedText);
    item.value = '';
  }

  // Add a new subtask and increase the points for the task.
  addSubtask(text: string) {
    if (text) {
      this.taskDetails.subtasks.push({ title: text, done: false, rewarded: false, points: this.subtaskPoints });
      this.taskDetails.points += this.subtaskPoints;
      this.taskProvider.saveData();
    }
  }

  // Toggle the checkbox and reward the user if thay haven't been already.
  checkboxTapped(subtask) {
    subtask.done = !subtask.done;
    if (!subtask.rewarded) {
      this.progressProvider.gainPoints(subtask);
      subtask.rewarded = true;
    }
    this.taskProvider.saveData();
  }

  // Toggle editmode which enables editing of titles and reordering/removing of subtasks.
  editmodeTapped() {
    this.notEditable = !this.notEditable;
    if (!this.notEditable) {
      this.editButtonText = 'Done';
    } else {
      this.editButtonText = 'Edit';
      this.checkSubtaskAdder();
    }
  }

  // Reorder subtasks to the provided indexes.
  reorderItems(indexes) {
    this.taskDetails.subtasks = reorderArray(this.taskDetails.subtasks, indexes);
    this.taskProvider.saveData();
  }

  // Remove a given subtask from the list.
  removeSubtask(subtask) {
    this.taskDetails.subtasks.splice(this.taskDetails.subtasks.indexOf(subtask), 1);
    // If the subtask hasn't already been checked, remove it's points from the task.
    if(!subtask.rewarded) { this.taskDetails.points -= this.subtaskPoints; }
    this.taskProvider.saveData();
  }

  // Update the title of a subtask. 
  changeSubtask(subtask, newTitle) {
    subtask.title = newTitle;
    this.taskProvider.saveData();
  }

  // Update a textfield with a given value.
  changeText(field, newText) {
    this.taskDetails[field] = newText;
    this.taskProvider.saveData();
  }

}
