import { Component } from '@angular/core';

import { NavController, ModalController, ActionSheetController, Platform, ToastController, AlertController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { TaskProvider } from '../../providers/task-provider';
import { ProgressProvider } from '../../providers/progress-provider';

import { Creator } from '../creator/creator';
import { TaskDetails } from '../task-details/task-details'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  editmode: boolean;

  constructor(public navCtrl: NavController, private modalCtrl: ModalController, public taskProvider: TaskProvider, private actionSheetCtrl: ActionSheetController, private platform: Platform, public progressProvider: ProgressProvider, private toastCtrl: ToastController, public alertCtrl: AlertController, public storage: Storage) {
    this.editmode = false;

    // If this is the first time the app is opened, prompt the user to type in a username.
    this.storage.get('first').then((data) => {
      if(!data) {
        this.promptUsername();
        this.storage.set('first', 'this is not the first time');
      }
    });
  }

  // Navigate to Creator.
  createTapped() {
    this.navCtrl.push(Creator);
  }

  // Check if all subtasks are done. If they are, the task is set to done and the user gains points. 
  checkboxTapped(task) {
    if (this.taskProvider.allDone(task)) {
      task.done = !task.done;
      if (!task.rewarded) {
        this.progressProvider.gainPoints(task);
        task.rewarded = true;
      }
      this.taskProvider.saveData();
    } else {
      if (!task.done) {
        let toast = this.toastCtrl.create({
          message: "This task isn't done yet",
          duration: 3000,
          position: 'bottom'
        });
        toast.present();
      }
      task.done = false;
    }
  }

  // Navigate to TaskDetails
  taskTapped(task) {
    this.navCtrl.push(TaskDetails, {
      task: task
    });
  }

  // Present an actionsheet with an option to delete the task, or to cancel.
  taskOptions(task) {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Delete',
          icon: !this.platform.is('ios') ? 'trash' : null,
          role: 'destructive',
          handler: () => {
            this.taskProvider.removeTask(task);
          }
        }, {
          text: 'Cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          role: 'cancel',
          handler: () => { }
        }
      ]
    });
    actionSheet.present();
  }

  // Tell taskprovider to reorder tasks according to the new indexes. 
  reorderItems(indexes) {
    this.taskProvider.reorderTasks(indexes);
  }

  // Toggle editmode, which shows indicators for reordering and removing tasks.
  editTapped() {
    this.editmode = !this.editmode;
  }

  // Tell taskprovider to remove a given task.
  removeTapped(task) {
    this.taskProvider.removeTask(task);
  }

  // Prompt user to type in a new username.
  promptUsername() {
      let prompt = this.alertCtrl.create({
      title: 'Username',
      message: "Enter the name that you want to be visible in your stats",
      inputs: [
        {
          name: 'username',
          placeholder: 'Name'
        },
      ],
      buttons: [
        {
          text: 'Save',
          handler: data => {
            this.progressProvider.setUserName(data.username);
          }
        }
      ]
    });
    prompt.present();
  }

}
