import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class About {

  slideOptions;
  firstVisit: boolean;

  constructor(public navCtrl: NavController, public storage: Storage) {}

  ionViewDidLoad() {
    // Display pager on slides.
    this.slideOptions = {
      pager: true
    };

    // If this is the first visit, set firstVisit to true, which shows a welcome slide.
    this.storage.get('first').then((data) => {
      if(!data) {
        this.firstVisit = true;
      } else {
        this.firstVisit = false;
      }
    });

  }

  // If the user clicks skip och continue, navigate to homepage.
  continue() {
    this.navCtrl.setRoot(HomePage);
  }

}
