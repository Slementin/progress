import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { ProgressProvider } from '../../providers/progress-provider';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class Profile {

  constructor(public navCtrl: NavController, public progressProvider: ProgressProvider, public alertCtrl: AlertController) {}

  // Tell ProgressProvider to update the username if the field is blurred.
  userNameBlurred(name) {
    this.progressProvider.setUserName(name);
  }

  // Warn the user about clearing the user data.
  clearClicked() {
    let confirm = this.alertCtrl.create({
      title: 'Clear all userdata',
      message: 'All your current progress will be lost. Are you sure you want to start over?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {}
        },
        {
          text: 'Yes',
          handler: () => {
            // Tell progressprovider to remove all progress.
            this.progressProvider.removeProgress();
          }
        }
      ]
    });
    confirm.present();
  }

}
