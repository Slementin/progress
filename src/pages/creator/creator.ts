import { Component } from '@angular/core';
import { NavController, reorderArray, ToastController } from 'ionic-angular';

import { TaskProvider } from '../../providers/task-provider';

import { Task } from '../../app/task';
import { Subtask } from '../../app/subtask';

@Component({
  selector: 'page-creator',
  templateUrl: 'creator.html'
})
export class Creator {

  task: Task;
  subtasks: Subtask[];

  points: number;
  subtaskPoints: number;

  done: boolean;

  listItemPlaceholder: string;

  constructor(public navCtrl: NavController, public taskProvider: TaskProvider, public toastCtrl: ToastController) {
    this.listItemPlaceholder = 'New item ...'
    this.points = 10;
    this.subtasks = [];
    this.done = false;
    this.subtaskPoints = this.taskProvider.subtaskPoints;
  }

  // Tell taskprovider to add a task.
  saveTapped(title: string, notes: string) {
    if(title !== '') {
      this.taskProvider.addTask({ title: title, points: this.points, subtasks: this.subtasks, notes: notes, done: this.done, rewarded: false });
      this.navCtrl.pop();
    } else {
      this.toast('A title is required in order to create a task');
    }
  }

  // Hide placeholder when the inputfield is focused. 
  inputFocused() {
    this.listItemPlaceholder = '';
  }

  // Add subtask when the textfield is blurred. Set placeholder.
  inputBlurred(item) {
    if (item.value) {
      this.addSubtask(item.value);
      item.value = '';
    }
    this.listItemPlaceholder = 'New item ...'
  }

  // Add subtask when the user hits enter.
  enterTapped(item) {
    let text = item.value;
    // Remove whitespace from the input text.
    let cleanedText = text.replace(/^\s+|\s+$/g, '');
    this.addSubtask(cleanedText);
    item.value = '';
  }

  // Reorder tasks according to the new indexes.
  reorderItems(indexes) {
    this.subtasks = reorderArray(this.subtasks, indexes);
  }

  // Push a new subtask to the list that will be contained within the task. Increase the amount of points the user will gain from the task.
  addSubtask(text: string) {
    if (text) {
      this.subtasks.push({ title: text, done: false, rewarded: false, points: this.subtaskPoints });
      this.points += this.subtaskPoints;
    }
  }

  // Update the title of the subtask.
  changeSubtask(subtask, newTitle) {
      subtask.title = newTitle;
  }

  // Remove the subtask from the list.
  removeSubtask(subtask) {
    this.subtasks.splice(this.subtasks.indexOf(subtask), 1);
    this.points -= this.subtaskPoints;
  }

  // Display toast with a given message.
  toast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}
