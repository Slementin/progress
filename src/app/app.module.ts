import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { Elastic } from 'angular2-elastic';

import { HomePage } from '../pages/home/home';
import { About } from '../pages/about/about';
import { Creator } from '../pages/creator/creator';
import { Profile } from '../pages/profile/profile';
import { TaskDetails } from '../pages/task-details/task-details';

import { TaskProvider } from '../providers/task-provider';
import { ProgressProvider } from '../providers/progress-provider';

import { Storage } from '@ionic/storage'

import { ProgressIndicator } from '../components/progress-indicator/progress-indicator';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    About,
    Creator,
    Profile,
    TaskDetails,
    ProgressIndicator
  ],
  imports: [
    Elastic,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    About,
    Creator,
    Profile,
    TaskDetails,
    ProgressIndicator
  ],
  providers: [
    TaskProvider,
    ProgressProvider,
    Storage
  ]
})
export class AppModule {}
