import { Subtask } from './subtask';

export class Task {
    title: string;
    points: number;
    subtasks: Subtask[];
    notes: string;
    done: boolean;
    rewarded: boolean;
}