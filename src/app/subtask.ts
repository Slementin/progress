export class Subtask {
    title: string;
    done: boolean;
    rewarded: boolean;
    points: number;
}