import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar } from 'ionic-native';
import { Storage } from '@ionic/storage';

import { HomePage } from '../pages/home/home';
import { Profile } from '../pages/profile/profile';
import { About } from '../pages/about/about';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage;
  profilePage: any;
  pages: Array<{title: string, component: any}>;

  constructor(private platform: Platform, private menu: MenuController, public storage: Storage) {
    this.initializeApp();
  }

  initializeApp() {
    this.profilePage = {title: 'Profile', component: Profile};
    this.pages = [ {title: 'Home', component: HomePage}, {title: 'About', component: About} ];
    
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });

    // Check if this is the first time the app is opended.
    this.storage.get('first').then((data) => {
      if(!data) {
        this.rootPage = About;
      } else {
        this.rootPage = HomePage;
      }
    });
  }

  // In the menu, navigate to the page that has been clicked.
  pageTapped(page) {
    this.menu.close();
    this.nav.setRoot(page.component);
  }
}
