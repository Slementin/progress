import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ToastController } from 'ionic-angular';

@Injectable()
export class ProgressProvider {

  progress: Object;

  userName: string;
  progressIndex: number; // The amount of progress the player has made, measured in percentage.
  progressPoints: number; // The amout of progress points the player has earned in total.
  playerLevel: number;
  doneTasks: number;
  fullLevel: number;

  constructor(public http: Http, public storage: Storage, public toastCtrl: ToastController) {
    this.initalizeValues();
  }

  // Set all standard values. If there's data in 'progress', set those values.
  initalizeValues() {
    this.fullLevel = 100;
    this.userName = '';
    this.progressIndex = 0;
    this.progressPoints = 0;
    this.playerLevel = 0;
    this.doneTasks = 0;
    this.progress = { userName: this.userName, progressIndex: this.progressIndex, progressPoints: this.progressPoints, playerLevel: this.playerLevel, doneTasks: this.doneTasks};

    this.storage.get('progress').then((data) => {
      if (data) {
        this.progress = data;
        this.userName = this.progress['userName'];
        this.progressIndex = this.progress['progressIndex'];
        this.progressPoints = this.progress['progressPoints'];
        this.playerLevel = this.progress['playerLevel'];
        this.doneTasks = this.progress['doneTasks'];
      }
    });
  }

  // Gain points every time a task that has not been rewarded is checked.
  gainPoints(task) {
    // Increase the number of total done tasks.
    this.doneTasks++;
    // Increase the number of total points earned.
    this.progressPoints += task.points;
    // calculate the amount of progress the player has made, in percentage.
    let percentage = (task.points / this.fullLevel) * 100;
    if (this.progressIndex + percentage >= 100) {
      // level up if the progressindex reaches over 100%
      this.playerLevel ++;
      this.toast('You just reached level ' + this.playerLevel + '!');
      this.progressIndex = (this.progressIndex + percentage) - 100;
    } else {
      this.progressIndex += percentage;
    }
    this.save(this.progress);
  }

  // Display a toast with the provided text.
  toast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  // The the username to the provided string. 
  setUserName(name: string) {
    this.userName = name;
    this.save(this.progress);
  }

  // Save prorgress in storage.
  save(data) {
    this.progress['userName'] = this.userName;
    this.progress['progressIndex'] = this.progressIndex;
    this.progress['progressPoints'] = this.progressPoints;
    this.progress['playerLevel'] = this.playerLevel;
    this.progress['doneTasks'] = this.doneTasks;

    this.storage.set('progress', this.progress);
  }

  // Remove progress from storage.
  removeProgress() {
    this.storage.remove('progress');
    this.initalizeValues();
  }

}
