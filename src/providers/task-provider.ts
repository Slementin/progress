import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { reorderArray } from 'ionic-angular'

import { Task } from '../app/task';

@Injectable()
export class TaskProvider {

  tasks: Task[];
  subtaskPoints: number;

  constructor(public http: Http, public storage: Storage) {
    this.tasks = [];
    this.subtaskPoints = 3;
    // Get tasks from storage.
    this.storage.get('tasks').then((data) => {
      if (data) {
        this.tasks = data;
      }
    });
  }

  // Push a task to the list and update storage.
  addTask(task: Task) {
    this.tasks.push(task);
    this.saveData();
  }

  // Remove a task from the list and update storage.
  removeTask(task) {
    this.tasks.splice(this.tasks.indexOf(task), 1);
    this.saveData();
  }

  // Reorder tasks to the given index.
  reorderTasks(indexes) {
    this.tasks = reorderArray(this.tasks, indexes);
    this.saveData();
  }

  // Check if all subtasks within a task are done.
  allDone(task) {
    for (let subtask of task.subtasks) {
      if (!subtask.done) {
        return false;
      }
    }
    return true;
  }

  // Save local storage.
  saveData() {
    this.storage.set('tasks', this.tasks);
  }

}
