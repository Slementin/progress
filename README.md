# Progress #

This is an application built in Ionic 2 that lets users create checklists. Each item in the list can contain a checklist of it's own. Every time the user checks off an item they gain some progress points. If they earn enough points they gain a level. Users also have their own profile where stats are displayed.

The idea behind this concept is to make it more fun and easy to get things done in life. When users can actually see the progress that they're making they get more motivated. The application also encourages users to break tasks down into smaller chunks, as this gives the them more points.

### Prerequisites

Follow this guide to get ionic running on your machine: 
http://ionicframework.com/docs/v2/setup/installation/

### Install node modules

```bash
$ npm install
$ npm install angular2-elastic
```

### Running in a browser

```bash
$ ionic serve
```

### Running on a device

Follow this guide to get the application running on either Android or iOS:
http://ionicframework.com/docs/v2/setup/deploying/